<?php
require_once 'registry.php';

function outputHeader() {
    $registry = Registry::getInstance();
    $c = $registry->get('color');
    $file = 'addZone.php';
    $msg = $file . str_repeat(' ', 80-strlen($file));
    echo $c($msg)->white()->bold()->highlight('green') . PHP_EOL;
}

function errorMsg($msg) {
    $registry = Registry::getInstance();
    $c = $registry->get('color');
    $msg = $msg . str_repeat(' ', 80-strlen($msg));
    echo $c($msg)->white()->bold()->highlight('red') . PHP_EOL;
}